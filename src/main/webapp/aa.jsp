<html lang="en" data-ng-app="myApp">
<head>
<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
<link
	href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css"
	rel="Stylesheet"></link>

<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<title>JSON Form</title>
<script type="text/javascript"
	src="https://ajax.googleapis.com/ajax/libs/angularjs/1.2.6/angular.min.js"></script>

</head>
<body>
	<form data-ng-submit="submit()" data-ng-controller="MyController"
		 >
		<h3>{{headerText}}</h3>
		{{formData}}<br> <br>
		<div data-ng-init="getCountry()">
			
			<select id="countrylist"
				style="border-radius: 10px; width: 210px; height: 40px"
				data-ng-model="Countryselected"
				data-ng-options=" country.cntryName for country in getCountries"
				data-ng-click="getCity()">
				<option value="">Select Country</option>
			
			</select>
		</div>
		<br>
		<div>
			<select style="border-radius: 10px; width: 210px; height: 40px"
				data-ng-model="cityselect" data-ng-disabled="!Countryselected"
				data-ng-options=" c.cityName for c in getCities " data-ng-change="getPlace()"
				>
				<option value="">Select City</option>
			</select>
		</div>

		<div>
			<p>{{Countryselected}}</p>
		</div>
		<div>
			<p>{{cityselect}}</p>
		</div>
		<div>
			
			    <pre>Form data ={{list}}</pre>
			    <br>
			    
			 <pre>Form data ={{list2}}</pre>
			
		</div>

		<h4>You submitted below data through post:</h4>
		<pre>Data ={{list}}</pre>
		<script type="text/javascript">
			var app = angular.module('myApp', []);
			app
					.controller(
							'MyController',
							function($scope, $http) {

								$scope.getCountry = function() {

									$http(
											{
												method : 'GET',
												url : 'http://localhost:8080/SpringRestCrud/newclue/country/list'
											}).success(
											function(data, status, headers,
													config) {
												$scope.getCountries = data;
											}).error(
											function(data, status, headers,
													config) {
												// called asynchronously if an error occurs
												// or server returns response with an error status.
											});
								};
								
							
								$scope.getCity = function() {
									//$scope.availableCities = [];

									$http(
											{
												method : 'GET',
												url : 'http://localhost:8080/SpringRestCrud/newclue/country/' + $scope.Countryselected.cntryCode  + '/cities'
											}).success(
											function(data, status, headers,
													config) {
												$scope.getCities = data;
											}).error(
											function(data, status, headers,
													config) {
												// called asynchronously if an error occurs
												// or server returns response with an error status.
											});
//  									angular
//  											.forEach(
//  													$scope.getCities,
// 													function(value) {
// 														if (value.cntryCode == $scope.Countryselected.cntryCode) {
//  															$scope.availableCities
//  																	.push(value);
//  														}

//  													});
								};
 								$scope.contry = angular.copy.Countryselected;
 								$scope.city = angular.copy.cityselect;

								//   --------------------------------------------------------------------------------------------          
								$scope.getPlace = function() {
									
									$scope.complete = function()

									{
										$scope.availablePlaces=[];
									
 										

										$http(
												{
													method : 'GET',
													url : 'http://localhost:8080/SpringRestCrud/newclue/cities/ '+  $scope.cityselect.cityCode  + '/clueanswers'
												}).success(
												function(data, status, headers,
														config) {
													  
                                                      $scope.getPlaces=data;
                                                     
                                                    


													
													
												
												}).error(
												function(data, status, headers,
														config) {
													// called asynchronously if an error occurs
													// or server returns response with an error status.
												});

										angular
												.forEach(
														$scope.getPlaces,
														function(value) {
															if (value.getPlaces !=' ') {
																$scope.availablePlaces
																		.push(value.answer);
															}

														});
										

													$("#tags").autocomplete(

															{

																	source : $scope.availablePlaces
													});

									}
									

								};
// 								------------------------------------------------------------------------------
								$scope.list2=[];
								$scope.list = [];
								$scope.headerText = 'AngularJS Post Form Spring MVC example: Submit below form';
								$scope.submit = function() {
								

									var countrydata = { 
											cntryName: $scope.Countryselected.cntryName,
										     cntryCode: $scope.Countryselected.cntryCode
										   
										     

									};
									
									alert("Hello2--->"+countrydata.cityName);
									var countrypost = $http
											.post(
													'http://localhost:8080/SpringRestCrud/newclue/countryData',
													countrydata);
									
									countrypost.success(function(data, status,
											headers, config) {
										alert("hello3--"+data.cntryCode);
										
										
										$scope.list.push(data);
									});
									countrypost.error(function(data, status,
											headers, config) {
										alert("Exception details: "
												+ JSON.stringify({
													data : data
												}));
									});

									//Empty list data after process
									$scope.list = [];
									
// 								-------------------------------city  post deatils----------------------------------------	
									var citydata = { 
											cityName: $scope.cityselect.cityName,
											cityCode: $scope.cityselect.cityCode
										   
										     

									};
									
									alert("Hello2--->"+citydata.cityName);
									var citypost = $http
											.post(
													'http://localhost:8080/SpringRestCrud/newclue/cityData',
													citydata);
									
									citypost.success(function(data, status,
											headers, config) {
										alert("hello3--"+data.cntryCode);
										
										
										$scope.list.push(data);
									});
									citypost.error(function(data, status,
											headers, config) {
										alert("Exception details: "
												+ JSON.stringify({
													data : data
												}));
									});

									//Empty list data after process
									$scope.list2 = [];

								};
                                   
							});
		</script>

		<input type="text" id="tags" data-ng-model="placeselected"
			data-ng-keydown="complete()" placeholder="Type place name" > <br>
<!-- 		   	 <select style="border-radius: 10px; width: 210px; height: 40px" data-ng-model="placeselected"  data-ng-options=" x.answer for x in getPlaces "> -->
<!-- 		                  <option value="">place</option> -->
<!-- 		                </select> -->
<!-- 		<input type="submit" value="Submit" id="submit"> -->
	</form>

</body>

</html>
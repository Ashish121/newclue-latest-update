package com.beingjavaguys.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
@Entity
@Table(name = "clue_answer")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Clueanswer implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "ans_Id")
	private long ansId;

	@Column(name = "answer")
	private String answer;
	
	@ManyToOne
	@JoinColumn(name="Answer_CityID", nullable=false)
	@JsonIgnore
	private Citycode citycode;
	
	
	
	//Hibernate requires no-args constructor
		
	public long getAnsId() {
		return ansId;
	}

	public void setAnsId(long ansId) {
		this.ansId = ansId;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}
	public Citycode getCitycode() {
		return citycode;
	}
	public void setCitycode(Citycode cart) {
		this.citycode = cart;
	}
	
}

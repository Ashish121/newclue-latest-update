package com.beingjavaguys.model;

import java.io.Serializable;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
@Entity
@Table(name = "city_code")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Citycode implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue
	@Column(name = "city_Code")
	private long cityCode;

	@Column(name = "city_Name")
	private String cityName;
	
	@ManyToOne
	@JoinColumn(name="Cntry_Code", nullable=false)
	@JsonIgnore
	private Countrycode countrycode;
	
	@OneToMany(mappedBy="citycode", fetch=FetchType.LAZY)
	@JsonIgnore
	private List<Clueanswer> clueanswer;
	
	
	//Hibernate requires no-args constructor
		
	public long getCityCode() {
		return cityCode;
	}

	public void setCityId(long cityCode) {
		this.cityCode = cityCode;
	}

	public String getCityName() {
		return cityName;
	}

	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public Countrycode getCountrycode() {
		return countrycode;
	}
	public void setCountrycode(Countrycode cart) {
		this.countrycode = cart;
	}
	public List<Clueanswer> getClueanswer() {
		return clueanswer;
	}

	public void setClueanswer(List<Clueanswer> clueanswer) {
		this.clueanswer = clueanswer;
	}
	
	
	
}

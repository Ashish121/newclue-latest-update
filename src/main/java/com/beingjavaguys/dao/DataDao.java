package com.beingjavaguys.dao;

import java.util.List;

import com.beingjavaguys.model.Citycode;
import com.beingjavaguys.model.Cluedata;
import com.beingjavaguys.model.Cluetags;
import com.beingjavaguys.model.Countrycode;
import com.beingjavaguys.model.Clueanswer;


public interface DataDao {

	public boolean addEntity(Countrycode country_code) throws Exception;
	public Countrycode getEntityById(long id) throws Exception;
	public List<Countrycode> getEntityList() throws Exception;
	public boolean deleteEntity(long id) throws Exception;
	
	public List<Citycode> getCities(long id) throws Exception;
	public List<Citycode> getEntityList1() throws Exception;
	
	public Cluedata getEntityById2(long id) throws Exception;
	public List<Cluedata> getEntityList2() throws Exception;
	
	public Cluetags getEntityById3(long id) throws Exception;
	public List<Cluetags> getEntityList3() throws Exception;
	
	public List<Clueanswer> getClueanswers(long id) throws Exception;
	public List<Clueanswer> getEntityList4() throws Exception;
	

	
	
	
}












































































































